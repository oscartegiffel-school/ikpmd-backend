<?php

namespace App\Http\Requests;

class StoreMessageRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer',
            'chat_room_id' => 'required|integer',
            'message' => 'required|string'
        ];
    }
}
