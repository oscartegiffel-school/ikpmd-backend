<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class ApiRequest extends FormRequest
{

     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Determine if the current request is asking for JSON in return.
     *
     * @return bool
     */
    public function wantsJson()
    {
        return true;
    }

    /**
     * For some calls the caller can present the url with a parameter called 'with'.
     * This parameters indicates the relations it wants with the requested recourse.
     * E.g. ?with=countries,locale on a '/users' call. This method will return these
     * relations as an array.
     *
     * @return array
     */
    public function with()
    {
        // Convert it to an array by exploding it by a comma.
        $with = $this->input('with') ? explode(',', $this->input('with')) : [];

        // Make sure it has unique contents.
        $with = array_unique($with);

        return $with;
    }
}
