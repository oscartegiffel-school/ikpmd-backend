<?php

namespace App\Http\Controllers;

use App\ChatRoom;
use App\Http\Requests\DestroyChatRoomRequest;
use App\Http\Requests\IndexChatRoomRequest;
use App\Http\Requests\ShowChatRoomRequest;
use App\Http\Requests\StoreChatRoomRequest;
use App\Http\Requests\UpdateChatRoomRequest;
use App\Http\Resources\ChatRoom as ChatRoomResource;

class ChatRoomsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexChatRoomRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(IndexChatRoomRequest $request)
    {
        return ChatRoomResource::collection(ChatRoom::with($request->with())->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreChatRoomRequest $request
     * @return ChatRoomResource
     */
    public function store(StoreChatRoomRequest $request)
    {
        return new ChatRoomResource(ChatRoom::create($request->all()));
    }

    /**
     * Shows a chat room
     *
     * @param ShowChatRoomRequest $request
     * @param ChatRoom $chatRoom
     * @return ChatRoomResource
     */
    public function show(ShowChatRoomRequest $request, ChatRoom $chatRoom)
    {
        return new ChatRoomResource($chatRoom->load($request->with()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateChatRoomRequest $request
     * @param ChatRoom $chatRoom
     * @return ChatRoomResource
     */
    public function update(UpdateChatRoomRequest $request, ChatRoom $chatRoom)
    {
        $chatRoom->update($request->all());
        return new ChatRoomResource($chatRoom);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyChatRoomRequest $request
     * @param ChatRoom $chatRoom
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(DestroyChatRoomRequest $request, ChatRoom $chatRoom)
    {
        $chatRoom->delete();
        return response()->json(null, 204);
    }
}
