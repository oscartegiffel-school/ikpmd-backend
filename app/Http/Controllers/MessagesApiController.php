<?php

namespace App\Http\Controllers;

use App\Http\Requests\DestroyMessageRequest;
use App\Http\Requests\StoreMessageRequest;
use App\Http\Resources\Message as MessageResource;
use App\Message;


class MessagesApiController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMessageRequest $request
     * @return MessageResource
     */
    public function store(StoreMessageRequest $request)
    {
        return new MessageResource(Message::create($request->all()));
    }

    /**
     * @param DestroyMessageRequest $request
     * @param Message $message
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroyMessageRequest $request, Message $message)
    {
        $message->delete();
        return response()->json(null, 204);
    }
}
