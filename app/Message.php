<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'chat_room_id', 'message',
    ];

    public function chatRoom() {
      return $this->hasOne('App\ChatRoom');
    }

    public function user() {
      return $this->hasOne('App\User');
    }


}
