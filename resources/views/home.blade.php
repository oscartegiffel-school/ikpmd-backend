@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>

            @role('admin')
            <!-- let people make clients -->
            <passport-clients></passport-clients>

            <!-- list of clients people have authorized to access our account -->
            <passport-authorized-clients></passport-authorized-clients>

            <!-- make it simple to generate a token right in the UI to play with -->
            <passport-personal-access-tokens></passport-personal-access-tokens>
            @endrole
        </div>
    </div>
</div>
@endsection
