<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create();

        // Add development accounts
        $user = factory(App\User::class)->create([
            "email" => "oscar@tegiffel.com",
            "username" => "oscarteg",
            "password" => bcrypt("welkom123"),
        ]);

        $user->assignRole('admin');

        $user = factory(App\User::class)->create([
            "email" => "guus1993@gmail.com",
            "username" => "guusseldenthuis",
            "password" => bcrypt("welkom123"),
        ]);

        $user->assignRole('admin');
    }
}
