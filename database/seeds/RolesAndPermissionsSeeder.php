<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'get all users']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'destroy message']);

        // create roles and assign existing permissions
        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('get all users');
        $role->givePermissionTo('update user');
        $role->givePermissionTo('destroy message');
    }
}