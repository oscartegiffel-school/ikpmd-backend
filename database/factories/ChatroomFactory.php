<?php

use Faker\Generator as Faker;

$factory->define(App\ChatRoom::class, function (Faker $faker) {
    return [
        'name' => $faker->words($nb = 3, true),
    ];
});
