<?php

use Faker\Generator as Faker;

$factory->define(App\Message::class, function (Faker $faker) {
    return [
      'message' => $faker->text,
      'chat_room_id' => factory(App\ChatRoom::class)->create()->id,
      'user_id' => factory(App\User::class)->create()->id,
    ];
});
