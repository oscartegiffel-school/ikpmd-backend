<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/users', 'UsersApiController@store');
Route::get('/chat-rooms', 'ChatRoomsApiController@index'); // for testing purposes
Route::middleware('auth:api')->group(function () {

    Route::post('/messages', 'MessagesApiController@store');

    Route::get('/users/me', 'UsersApiController@showMe');
    Route::put('/users/me', 'UsersApiController@updateMe');

    // Route::get('/chatRooms', 'ChatRoomsApiController@index');
    Route::post('/chat-rooms', 'ChatRoomsApiController@store');
    Route::get('/chat-rooms/{chatRoom}', 'ChatRoomsApiController@show');

    Route::middleware('role:admin')->group(function () {

        // Messages calls
        Route::delete('/messages', 'MessagesApiController@destroy');

        // Users calls
        Route::get('/users', 'UsersApiController@index');
        Route::get('/users/{user}', 'UsersApiController@show');
        Route::put('/users/{user}', 'UsersApiController@update');
        Route::delete('/users/{user}', 'UsersApiController@destroy');

        // ChatRooms calls
        Route::put('/chat-rooms/{chatRoom}', 'ChatRoomsApiController@update');
        Route::delete('/chat-rooms/{chatRoom}', 'ChatRoomsApiController@destroy');
    });
});

